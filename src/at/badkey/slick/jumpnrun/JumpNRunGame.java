package at.badkey.slick.jumpnrun;

import org.newdawn.slick.Animation;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

import at.badkey.slick.jumpnrun.objects.Homer;

public class JumpNRunGame extends BasicGame{
	
	private Image background;
	public static final int WIDTH = 1920;
	public static final int HEIGHT = 1080;
	
	//Characters
	Character homer;
	
	public JumpNRunGame() {
		super("JumpNRun");
		// TODO Auto-generated constructor stub
	}

	@Override
	public void render(GameContainer arg0, Graphics g) throws SlickException {
		background.draw(0, 0);
		//Draw character
		homer.draw(g);
	}

	@Override
	public void init(GameContainer arg0) throws SlickException {
		//Init the background
		background = new Image("jumpnrun/img/background_neu.jpg");
		
		//Init characters
		homer = new Homer(10,10);
	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		//Check for character key inputs
		homer.move(delta, gc);
		
	}
	
	public static void main(String[] args) throws SlickException {

		AppGameContainer jumpnrun = new AppGameContainer(new JumpNRunGame());

		jumpnrun.setDisplayMode(WIDTH, HEIGHT, false);

		jumpnrun.setShowFPS(true);
		jumpnrun.setTargetFrameRate(60);
		jumpnrun.start();

  }

}
