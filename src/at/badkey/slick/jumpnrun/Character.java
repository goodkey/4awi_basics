package at.badkey.slick.jumpnrun;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public interface Character {
	
	public void draw(Graphics g);
	public void move(int delta, GameContainer gc);
}
